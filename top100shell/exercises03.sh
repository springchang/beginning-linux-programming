#!/bin/bash
#
# author 张翠山
# 每周5使用tar命令备份/var/log下的所有日志文件

tar -czf log-`date +%Y%m%d`.tar.gz /var/log
